// Helper для преобразования даты в квартал-год
function DateToQuarter (IsoStringDate) {
  // парсим ISO Date в Date
  const timeFormat = new Date(IsoStringDate)
  // определяем номер квартала по формуле
  const quarter = Math.floor((timeFormat.getMonth() + 3) / 3)
  return `${timeFormat.getFullYear()} Квартал ${quarter}`
}
// Helper для сортировки массива с объектами
function compare (a, b) {
  if (a.x < b.x) {
    return -1
  }
  if (a.x > b.x) {
    return 1
  }
  // a должно быть равным b
  return 0
}

const DealsBase = Object.create({}, {
  soldFact: { // сделка в статусе "Продажа"
    get: function () {
      return this.STAGE_ID === 'WON'
    }
  },
  // возвращает квартал в котором закрыта продажа сделки
  soldQuarter: {
    get: function () {
      // если поле даты закрытия пустое, то возвращаем 'Нет даты'
      return ('CLOSEDATE' in this && this.CLOSEDATE.length) ? DateToQuarter(this.CLOSEDATE) : 'Нет даты'
    }
  },
  groupedSectors: {
    get: function () {
      // определяем группу, к которому относится отрасль
      let res = 'Остальное'
      const groups = {
        774: 'Сервис', // Администрация города или региона
        776: 'Нефтегаз', // ВУЗ Нефтегазовая отрасль
        778: 'Производство', // ВУЗ Производство
        780: 'Сервис', // ВУЗ Сервис
        782: 'Сервис', // Медицина
        58: 'Нефтегаз', // Нефтегазовая отрасль
        60: 'Производство', // Производство
        848: 'Производство', // РЦК
        62: 'Сервис', // Сервис
        850: 'Сервис', // Физлица
        788: 'Производство' // Энергетика
      }
      // отраслей у сделки может быть несколько, проверяем каждую, последняя будет принята за истину
      this.UF_CRM_1614619275400.forEach(item => {
        if (item in groups) res = groups[item]
      })
      return res
    }
  }
})

const CRMDataBase = Object.create(null, {
  amountInBatch: { value: 50 }, // максимальный размер batch
  guideUserField: { value: [], writable: true }, // справочник с пользовательскими полями
  statusSaleInWork: { value: ['NEW', 'PROPOSAL', 'NEGOTIATION', '1', '2', '3', '4', '5', '6'] }, // статус сделка в работе
  statusSaleOnSigning: { value: ['1', '2'] }, // статус Договор на подписании (Договорная работа запущена, Договор отправлен)
  init: {
    value: function () {
    // загрузка справочника с пользовательскими полями
      BX24.callMethod('crm.deal.userfield.list', {}, (result) => {
        if (!result.error()) {
          this.guideUserField = this.guideUserField.concat(result.data())
          if (result.more()) result.next()
        }
      })
      // загрузка списка сделок
      const options = {
      // order: { "CLOSEDATE": "ASC" },
      // filter: { "CLOSED": "YES" },
        select: ['ID'] // загружаю только ID-шники
      }
      BX24.callMethod('crm.deal.list', options,
        (result) => {
          if (!result.error()) {
          // на основе списка ID-шников создаем набор параметров для batch метода
            const mas = result.data().map(item => [item.ID, {
              method: 'crm.deal.get',
              params: { ID: item.ID }
            }])
            // ему нужен array
            const options = Object.fromEntries(mas)
            BX24.callBatch(options, (result) => {
              Object.values(result).forEach(item => {
              // создаём объект с прототипом
                const newObj = Object.setPrototypeOf(item.data(), DealsBase)
                // console.log('загружена сделка ',newObj.soldQuarter);
                this.deals.push(newObj)
              })
            })
            // console.log('result.total()', result.total());
            // прогресс загрузки данных
            if (!result.total()) {
              this.loadingDataProcess = 100
            } else if (!result.query.start) {
              this.loadingDataProcess = 0
            } else if ((result.total() - result.query.start) < this.amountInBatch) {
              this.loadingDataProcess = 100
            } else {
              this.loadingDataProcess = Math.floor(result.query.start / result.total() * 100)
            }
            if (result.more()) result.next()
          } else { // если ошибка, то выводим окно ошибки
            this.errorStatus = result.error()
          }
        }
      )
    }
  },
  // собирает сделки, которые имеют статус Продажа
  salesData: {
    get: function () {
      return this.deals.filter(item => item.soldFact)
    }
  },
  // собирает сделки со средней вероятностью и статусом В работе
  probabilityMediumDeals: {
    get: function () {
      return this.deals.filter(item => item.UF_CRM_1614619470704 === '72' && this.statusSaleInWork.includes(item.STAGE_ID))
    }
  },
  // собирает сделки со средней вероятностью и статусом В работе
  probabilityHighDeals: {
    get: function () {
      return this.deals.filter(item => item.UF_CRM_1614619470704 === '74' && this.statusSaleInWork.includes(item.STAGE_ID))
    }
  },
  // собирает сделки на подписании
  onSigningDeals: {
    get: function () {
      return this.deals.filter(item => this.statusSaleOnSigning.includes(item.STAGE_ID))
    }
  },
  // метод собирает какие кварталы должны участвовать в графике Продаж (Ось Y), без него apexchart работает криво
  salesDataQuarters: {
    get: function () {
      // собираются уникальные значения поля Квартал продажи
      const AllQuarters = new Set(this.salesData.map(item => item.soldQuarter))
      return [...AllQuarters] // преобразуем в массив
    }
  },
  // статический метод - выдаёт значение из справочника пользовательских полей
  getTypeOfUserField: {
    value: function (userField, value) {
      const a = this.guideUserField.find(item => item.FIELD_NAME === userField)
      const b = a.LIST.find(item => parseInt(item.ID) === value)
      return b?.VALUE ?? 'Не установлено в CRM'
    }
  },
  // статический метод для группировки сделок по кварталам. На входе массив сделок
  groupingByQuarter: {
    value: function (deals, typeOut = 'summa') {
      // суммирующая группировка по кварталам, используется поле soldQuarter
      const quarterSumma = deals.reduce((previousValue, item) => {
        if (item.soldQuarter) {
          const addToSumma = parseFloat(item.OPPORTUNITY / 1000 ?? '0')
          const newSumma = previousValue[item.soldQuarter].summa += addToSumma
          const newCount = previousValue[item.soldQuarter].count += 1
          const newValue = {
            summa: newSumma, // сумма
            count: newCount, // кол-во
            average: newSumma / newCount // среднее арифметическое
          }
          return Object.assign(previousValue, { [item.soldQuarter]: newValue })
        } else return previousValue
      }, // в начальное значение для reduce прописываем объект, где всевозможные кварталы имеют значение 0
      Object.fromEntries(this.salesDataQuarters.map(item => [item, { summa: 0, count: 0, average: 0 }])))
      return Object.entries(quarterSumma)
        .map(item => {
          let result // в зависимости от typeOut выдаём соответствующее значение
          if (typeOut === 'amount') result = item[1].count
          else if (typeOut === 'average') result = Math.round(item[1].average)
          else result = Math.round(item[1].summa)
          return { x: item[0], y: result }
        })
        .sort(compare) // сортировка на выходе
    }
  },
  // статический метод, считает сумму сделок по всем свойствам объекта, поданного на вход
  sumDeals: {
    value: function (objDeals) {
      const res = {}
      Object.entries(objDeals).forEach(elem => {
        res[elem[0]] = elem[1].reduce((prev, item) => prev + parseInt(item.OPPORTUNITY / 1000 ?? '0'), 0)
      })
      return res
    }
  },
  // статический метод, считает количество сделок, по всем свойствам объекта, поданного на вход
  countDeals: {
    value: function (objDeals) {
      const res = {}
      Object.entries(objDeals).forEach(elem => {
        res[elem[0]] = elem[1].length
      })
      return res
    }
  },
  // статический метод для группировки сделок по userField. Deals - массив сделок. UserField- поле для группировки
  groupingByUserField: {
    value: function (deals, userField) {
      if (!userField) return { 'Все категории': deals }
      else if (userField === 'sectorsGrouped') { // для группировки по группам отраслей
      // получаем список всех возможных типов
        const AllTypes = new Set(deals.map(item => item.groupedSectors))
        // создаём результирующий  объект
        const res = {}
        AllTypes.forEach(group => {
          res[group] = deals.filter(item => item.groupedSectors === group)
        })
        return res
      } else {
      // получаем список всех возможных типов
        const AllTypes = new Set(deals.map(item => item[userField][0]))
        // создаём результирующий  объект
        const res = {}
        AllTypes.forEach(itemOfUserFields => {
        // getTypeOfUserField - берет из справочника название категории по номеру и группы категории
          res[this.getTypeOfUserField(userField, itemOfUserFields)] = deals.filter(item => item[userField][0] === itemOfUserFields)
        })
        return res
      }
    }
  },
  // статический метод Отчёт продажи - подготовка данных для графиков
  prepareSalesReport: {
    value: function (deals, typeOfGrouping, typeOut) {
      // берем продажи и группируем их по указанному типу
      const grouped = this.groupingByUserField(deals, typeOfGrouping)
      console.log('prepareSalesReport grouped', grouped)
      // в каждой отрасли группируем по кварталам
      return Object.entries(grouped).map(item => {
        return {
          name: item[0],
          data: this.groupingByQuarter(item[1], typeOut)
        }
      })
    }
  },
  // Отчёт Продажи - группировка по отраслям
  salesQuarterBySectors: {
    get: function () {
      const type = 'UF_CRM_1614619275400' // кастомное поле, тип Отрасли
      return this.prepareSalesReport(this.salesData, type)
    }
  },
  // Отчёт Продажи - группировка по Группам отраслей
  salesQuarterBySectorsGrouped: {
    get: function () {
      const type = 'sectorsGrouped' // тип ввели внутри кода для группировки отраслей
      const a = this.prepareSalesReport(this.salesData, type)
      console.log('salesQuarterByGroupedSectors', a)
      return a
    }
  },
  // Отчёт Продажи - группировка по отраслям в штуках
  salesQuarterBySectorsAmount: {
    get: function () {
      const type = 'UF_CRM_1614619275400' // кастомное поле, тип Отрасли
      return this.prepareSalesReport(this.salesData, type, 'amount')
    }
  },
  // Отчёт Продажи - группировка  по Группам отраслей в штуках
  salesQuarterBySectorsAmountGrouped: {
    get: function () {
      const type = 'sectorsGrouped' // кастомное поле, тип Отрасли
      return this.prepareSalesReport(this.salesData, type, 'amount')
    }
  },
  // Отчёт Продажи - группировка по продуктам
  salesQuarterByProducts: {
    get: function () {
      const type = 'UF_CRM_1614618963661' // кастомное поле, тип Продукт
      return this.prepareSalesReport(this.salesData, type)
    }
  },
  // Отчёт Продажи - группировка по продуктам в штуках
  salesQuarterByProductsAmount: {
    get: function () {
      const type = 'UF_CRM_1614618963661' // кастомное поле, тип Продукт
      return this.prepareSalesReport(this.salesData, type, 'amount')
    }
  },

  // Отчёт Средний чек - группировка по продуктам
  averageCheckQuarterByProducts: {
    get: function () {
      const type = 'UF_CRM_1614618963661' // кастомное поле, тип Продукт
      return this.prepareSalesReport(this.salesData, type, 'average')
    }
  },

  // Отчёт  Средний чек  - без группировки
  averageCheckQuarter: {
    get: function () {
      return this.prepareSalesReport(this.salesData, null, 'average')
    }
  },

  // Отчёт Средний чек - группировка по отраслям
  averageCheckBySectors: {
    get: function () {
      const type = 'UF_CRM_1614619275400' // кастомное поле, тип Отрасли
      return this.prepareSalesReport(this.salesData, type, 'average')
    }
  },

  // Отчёт Продажи - группировка по Группам отраслей
  averageCheckQuarterBySectorsGrouped: {
    get: function () {
      const type = 'sectorsGrouped' // тип ввели внутри кода для группировки отраслей
      const a = this.prepareSalesReport(this.salesData, type, 'average')
      console.log('averageCheckByGroupedSectors', a)
      return a
    }
  },

  // Отчёт Продажи - без группировки
  salesQuarter: {
    get: function () {
      return this.prepareSalesReport(this.salesData)
    }
  },
  // Отчёт Продажи - без группировки в штуках
  salesQuarterAmount: {
    get: function () {
      return this.prepareSalesReport(this.salesData, null, 'amount')
    }
  },
  // статический метод для отчёта по потенциальным продажам
  prepareReportPotentialSales: {
    value: function (typeOut, param) {
      // переключатель между типом вывода данных: сумма или количество
      const typesOut = { summa: this.sumDeals, amount: this.countDeals }
      // результирующий объект
      const resObj = {
        medium: typesOut[typeOut](this.groupingByUserField(this.probabilityMediumDeals, param)),
        high: typesOut[typeOut](this.groupingByUserField(this.probabilityHighDeals, param)),
        onSigning: typesOut[typeOut](this.groupingByUserField(this.onSigningDeals, param))
      }
      // результирующий массив
      const resArr = []
      Object.entries(resObj).forEach(item => {
        Object.entries(item[1]).forEach(elem => {
          const ind = resArr.findIndex(fElem => fElem.category === elem[0])
          if (ind < 0) {
            resArr.push({ category: elem[0], [item[0]]: elem[1].toLocaleString('ru-RU') })
          } else resArr[ind][item[0]] = elem[1].toLocaleString('ru-RU')
        })
      })
      return resArr
    }
  },
  // Отчёт по потенциальным продажам без группировки в рублях
  potentialSalesAll: {
    get: function () {
      return this.prepareReportPotentialSales('summa')
    }
  },
  // Отчёт по потенциальным продажам без группировки в количестве
  potentialSalesAllAmount: {
    get: function () {
      return this.prepareReportPotentialSales('amount')
    }
  },
  // Отчёт по потенциальным продажам группировка по типу Отрасли в рублях
  potentialSalesSectors: {
    get: function () {
      const param = 'UF_CRM_1614619275400'
      return this.prepareReportPotentialSales('summa', param)
    }
  },
  // Отчёт по потенциальным продажам группировка по группам Отраслей в рублях
  potentialSalesSectorsGrouped: {
    get: function () {
      const param = 'sectorsGrouped'
      return this.prepareReportPotentialSales('summa', param)
    }
  },
  // Отчёт по потенциальным продажам группировка по типу Отрасли в количестве
  potentialSalesSectorsAmount: {
    get: function () {
      const param = 'UF_CRM_1614619275400'
      return this.prepareReportPotentialSales('amount', param)
    }
  },
  // Отчёт по потенциальным продажам группировка по группам отраслей в количестве
  potentialSalesSectorsAmountGrouped: {
    get: function () {
      const param = 'sectorsGrouped'
      return this.prepareReportPotentialSales('amount', param)
    }
  },
  //
  // Отчёт по потенциальным продажам группировка по типу Продукта в рублях
  potentialSalesProduct: {
    get: function () {
      const param = 'UF_CRM_1614618963661'
      return this.prepareReportPotentialSales('summa', param)
    }
  },
  // Отчёт по потенциальным продажам группировка по типу Продукта в количестве
  potentialSalesProductAmount: {
    get: function () {
      const param = 'UF_CRM_1614618963661'
      return this.prepareReportPotentialSales('amount', param)
    }
  }

})

// конструктор объекта с набором данных из CRM
function CRMData () {
  Object.setPrototypeOf(this, CRMDataBase)
  // для реактивности необходимо задать свойства экземпляру объекта
  this.loadingDataProcess = 0 // прогресс загрузки данных
  this.errorStatus = undefined // статус ошибки
  this.size = undefined // размеры фрейма
  this.deals = [] // сделки
  this.leads = [] // лиды
  this.init()
}
