const AvrgCheck = {
  template:
      '<v-tabs-items v-model="currentTab">' +
      '<v-tab-item v-for="item in structure" :key="item.id" :value="item.id">' +
      '<v-container>' +
      '<v-row>' +
      '<v-col cols="12">' +
      '<apexchart type="bar" :height="chartHeight" :options="optionsSumma" :series="item.series"/>' +
      '</v-col>' +
      '</v-row>' +
      '</v-container>' +
      '</v-tab-item>' +
      '</v-tabs-items>',
  components: {
    apexchart: VueApexCharts
  },
  props: { crmData: { type: Object, required: true }, outPutId: { type: Number, required: true } },
  data: function () {
    return {
      // общие параметры чарта
      chartOptions: {
        chart: {
          animations: {
            initialAnimation: {
              enabled: true
            }
          },
          toolbar: {
            show: false
          }
        },
        xaxis: {
          type: 'category'
        },
        dataLabels: {
          enabled: false
        },
        title: {
          text: 'Фактические продажи - средний чек',
          align: 'center',
          style: {
            fontSize: '16px',
            fontWeight: 'bold',
            fontFamily: 'Roboto, sans-serif'
          }
        }
      },
      // структура каждого таба
      structure: [
        {
          id: 0,
          menuButton: 'Все', // передаётся наружу
          series: this.crmData.averageCheckQuarter // в рублях
        },
        {
          id: 1,
          menuButton: 'По продуктам',
          series: this.crmData.averageCheckQuarterByProducts
        },
        {
          id: 2,
          menuButton: 'По отраслям',
          series: this.crmData.averageCheckBySectors
        },
        {
          id: 3,
          menuButton: 'По группам отраслей',
          series: this.crmData.averageCheckQuarterBySectorsGrouped
        }
      ],
      currentTab: this.outPutId // текущий таб
    }
  },
  methods: {
    // форматирование когда вводится сумма сделок в миллионах
    formatterSummaMln (val) {
      return `${(val / 1000).toLocaleString('ru-RU')} млн. руб.`
    },
    // форматирование когда вводится сумма сделок в тысячах
    formatterSummaThs (val) {
      return `${(val).toLocaleString('ru-RU')} тыс. руб.`
    },
    // форматирование когда вводится количество сделок
    formatterAmount (val) {
      return `${val} шт.`
    }

  },
  computed: {
    // параметры чарта при режиме сумма
    optionsSumma () {
      return Object.assign({}, this.chartOptions, {
        yaxis: {
          labels: {
            formatter: this.formatterSummaMln
          }
        },
        tooltip: {
          y: {
            formatter: this.formatterSummaThs
          }
        }
      })
    },
    chartHeight () {
      // задаём высоту чарта относительно высоты фрейма
      return this.crmData.size.scrollHeight * 0.5
    }
  },
  watch: { // при переключении табов снаружи переключаем экраны внутри
    outPutId (newValue) {
      this.currentTab = newValue
    },
    currentTab (newValue) { // при пролистывания табов отправляем инфу родителю
      this.$emit('change-tab', newValue)
    }
  },
  created () { // после создания компонента отправляем данные родителю о структуре табов
    this.$emit('loaded', { menu: this.structure, switcher: false })
  }

}
