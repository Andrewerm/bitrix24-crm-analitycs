const Sales = {
  template:
            '<v-tabs-items v-model="currentTab">' +
                '<v-tab-item v-for="item in structure" :key="item.id" :value="item.id">' +
                '<v-container>' +
                    '<v-row>' +
                        '<v-col cols="12">' +
                            '<apexchart type="bar" :height="chartHeight" :options="switcher?optionsAmount:optionsSumma" :series="switcher?item.seriesAmount:item.series"/>' +
                        '</v-col>' +
                        '<v-col cols="12">' +
                            '<v-data-table hide-default-footer caption="Потенциальные продажи" :headers="headers"  :items="switcher?item.potentialAmount:item.potential"/>' +
                        '</v-col>' +
                    '</v-row>' +
                '</v-container>' +
                '</v-tab-item>' +
            '</v-tabs-items>',
  components: {
    apexchart: VueApexCharts
  },
  props: { crmData: { type: Object, required: true }, outPutId: { type: Number, required: true }, switcher: { type: Boolean, required: true } },
  data: function () {
    return {
      // общие параметры чарта
      chartOptions: {
        chart: {
          animations: {
            initialAnimation: {
              enabled: true
            }
          },
          toolbar: {
            show: false
          }
        },
        xaxis: {
          type: 'category'
        },
        dataLabels: {
          enabled: false
        },
        title: {
          text: 'Фактические продажи',
          align: 'center',
          style: {
            fontSize: '16px',
            fontWeight: 'bold',
            fontFamily: 'Roboto, sans-serif'
          }
        }
      },
      headers: [
        { text: 'Категория', align: 'center', sortable: true, value: 'category' },
        { text: 'Средняя вероятность', align: 'center', sortable: true, value: 'medium' },
        { text: 'Высокая вероятность', align: 'center', sortable: true, value: 'high' },
        { text: 'На подписании', align: 'center', sortable: true, value: 'onSigning' }
      ],
      // структура каждого таба
      structure: [
        {
          id: 0,
          menuButton: 'Все', // передаётся наружу
          series: this.crmData.salesQuarter, // в рублях
          seriesAmount: this.crmData.salesQuarterAmount, // в количестве
          potential: this.crmData.potentialSalesAll, // в рублях
          potentialAmount: this.crmData.potentialSalesAllAmount // в количестве
        },
        {
          id: 1,
          menuButton: 'По продуктам',
          // switcher: false,
          series: this.crmData.salesQuarterByProducts,
          seriesAmount: this.crmData.salesQuarterByProductsAmount,
          potential: this.crmData.potentialSalesProduct,
          potentialAmount: this.crmData.potentialSalesProductAmount
        },
        {
          id: 2,
          menuButton: 'По отраслям',
          series: this.crmData.salesQuarterBySectors,
          seriesAmount: this.crmData.salesQuarterBySectorsAmount,
          potential: this.crmData.potentialSalesSectors,
          potentialAmount: this.crmData.potentialSalesSectorsAmount
        },
        {
          id: 3,
          menuButton: 'По группам отраслей',
          series: this.crmData.salesQuarterBySectorsGrouped,
          seriesAmount: this.crmData.salesQuarterBySectorsAmountGrouped,
          potential: this.crmData.potentialSalesSectorsGrouped,
          potentialAmount: this.crmData.potentialSalesSectorsAmountGrouped
        }
      ],
      currentTab: this.outPutId // текущий таб
    }
  },
  methods: {
    // форматирование когда вводится сумма сделок в миллионах
    formatterSummaMln (val) {
      return `${(val / 1000).toLocaleString('ru-RU')} млн. руб.`
    },
    // форматирование когда вводится сумма сделок в тысячах
    formatterSummaThs (val) {
      return `${(val).toLocaleString('ru-RU')} тыс. руб.`
    },
    // форматирование когда вводится количество сделок
    formatterAmount (val) {
      return `${val} шт.`
    }

  },
  computed: {
    // параметры чарта при режиме сумма
    optionsSumma () {
      return Object.assign({}, this.chartOptions, {
        yaxis: {
          labels: {
            formatter: this.formatterSummaMln
          }
        },
        tooltip: {
          y: {
            formatter: this.formatterSummaThs
          }
        }
      })
    },
    // параметры чарта при режиме количество
    optionsAmount () {
      return Object.assign({}, this.chartOptions, {
        yaxis: {
          labels: {
            formatter: this.formatterAmount
          }
        },
        tooltip: {
          y: {
            formatter: this.formatterAmount
          }
        }
      })
    },
    chartHeight () {
      // задаём высоту чарта относительно высоты фрейма
      return this.crmData.size.scrollHeight * 0.5
    }
  },
  watch: { // при переключении табов снаружи переключаем экраны внутри
    outPutId (newValue) {
      this.currentTab = newValue
    },
    currentTab (newValue) { // при пролистывания табов отправляем инфу родителю
      this.$emit('change-tab', newValue)
    }
  },
  created () {
    this.$emit('loaded', { menu: this.structure, switcher: true })
  }

}
